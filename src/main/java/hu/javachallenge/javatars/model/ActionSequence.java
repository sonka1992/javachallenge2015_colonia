package hu.javachallenge.javatars.model;

import hu.javachallenge.javatars.util.Action;
import hu.javachallenge.javatars.util.Coordinate;
import java.util.ArrayList;
import java.util.List;

public class ActionSequence {

    public ActionSequence() {
    }

    public ActionSequence(Coordinate targetCoordinate, List<Action> listOfActions) {
        this.targetCoordinate = targetCoordinate;
        this.listOfActions = listOfActions;
    }
    
    private Coordinate targetCoordinate;
    private List<Action> listOfActions = new ArrayList<Action>();

    public Coordinate getTargetCoordinate() {
        return targetCoordinate;
    }

    public void setTargetCoordinate(Coordinate targetCoordinate) {
        this.targetCoordinate = targetCoordinate;
    }

    public List<Action> getListOfActions() {
        return listOfActions;
    }

    public void setListOfActions(List<Action> listOfActions) {
        this.listOfActions = listOfActions;
    }
    
}
