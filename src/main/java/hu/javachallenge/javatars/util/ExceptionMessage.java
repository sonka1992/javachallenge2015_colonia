package hu.javachallenge.javatars.util;

public enum ExceptionMessage {
	
	NoCellInformation("Warning! Type of the target cell is unknown"),
	EnemyTerritory("Warning! Enemy territory ahead"),
	UnreachableCell("Warning! Unreachable cell"),
	BlockedCell("Warning! Cell is blocked by Builder unit or shuttle"),
	
	CannotStructureGranite("Warning! Cannot structure granite"),
	CannotStructureShuttle("Warning! Cannot structure shuttle"),
	AlreadyStructuredCell("Warning! Cell has already structured"),
	
	NotEnoughExplosive("Warning! Not enough explosive"),
	AlreadyExplodedCell("Warning! Cell has already exploded"),
	CannotExplodeObsidian("Warning! Obsidian isn't explodable");
	
	private String msg;
	
	private ExceptionMessage(String msg){
		this.msg = msg;
	}
	
	public String getMessage(){
		return msg;
	}
}
