package hu.javachallenge.javatars.util;

import eu.loxon.centralcontrol.WsCoordinate;
import eu.loxon.centralcontrol.WsDirection;

public class Coordinate extends WsCoordinate implements Comparable<Coordinate> {

    public Coordinate(WsCoordinate actPos, WsDirection dir) {
        super();

        switch (dir) {
            case UP:
                x = actPos.getX();
                y = actPos.getY() + 1;
                break;
            case RIGHT:
                x = actPos.getX() + 1;
                y = actPos.getY();
                break;
            case DOWN:
                x = actPos.getX();
                y = actPos.getY() - 1;
                break;
            case LEFT:
                x = actPos.getX() - 1;
                y = actPos.getY();
                break;
        }
    }

    public Coordinate(WsCoordinate cord) {
        super();
        x = cord.getX();
        y = cord.getY();
    }

    public Coordinate(int x, int y) {
        super();
        this.x = x;
        this.y = y;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + x;
        result = prime * result + y;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Coordinate other = (Coordinate) obj;
        if (x != other.x) {
            return false;
        }
        if (y != other.y) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(Coordinate o) {
        if (this.x != o.x) {
            return x - o.x;
        } else {
            return y - o.y;
        }
    }

    @Override
    public String toString() {
        return " x: " + x + " y: " + y + " ";
    }

}
