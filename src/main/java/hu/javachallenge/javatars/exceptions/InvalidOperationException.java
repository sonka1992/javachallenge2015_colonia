package hu.javachallenge.javatars.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hu.javachallenge.javatars.util.ExceptionMessage;

public class InvalidOperationException extends RuntimeException {

	private static final long serialVersionUID = -4233396984881783342L;
	private final Logger logger = LoggerFactory.getLogger(InvalidOperationException.class);
	 
	public InvalidOperationException(){
		
	}
	
	public InvalidOperationException(ExceptionMessage msg){
		super(msg.getMessage());
	}
	
	@Override
	public void printStackTrace() {
		logger.warn(getMessage());
		//super.printStackTrace();
	}

}
