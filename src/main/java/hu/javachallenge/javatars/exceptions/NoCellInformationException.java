package hu.javachallenge.javatars.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hu.javachallenge.javatars.util.ExceptionMessage;

public class NoCellInformationException extends RuntimeException {
	
	private static final long serialVersionUID = -5342319577717269057L;
	private final Logger logger = LoggerFactory.getLogger(NoCellInformationException.class);

	public NoCellInformationException(){
		
	}
	
	public NoCellInformationException(ExceptionMessage msg){
		super(msg.getMessage());
	}
	
	@Override
	public void printStackTrace() {
		logger.warn(getMessage());
		//super.printStackTrace();
	}
}
