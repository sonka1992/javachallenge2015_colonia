package hu.javachallenge.javatars.algorithms;

import eu.loxon.centralcontrol.ObjectType;
import eu.loxon.centralcontrol.WsBuilderunit;
import eu.loxon.centralcontrol.WsCoordinate;
import eu.loxon.centralcontrol.WsDirection;
import hu.javachallenge.javatars.accessor.ManagedOperationInvoker;
import hu.javachallenge.javatars.model.ActionSequence;
import hu.javachallenge.javatars.util.Action;
import hu.javachallenge.javatars.util.CellInformation;
import hu.javachallenge.javatars.util.Coordinate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimpleSpaceShuttle extends SpaceShuttle {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleSpaceShuttle.class);

    public SimpleSpaceShuttle(ManagedOperationInvoker invoker) {
        actionCost = new HashMap<>();
        gameMap = new TreeMap<>();
        units = new HashMap<>();
        this.invoker = invoker;
        this.setActualBuilderId(Integer.MIN_VALUE);
        invoker.setSpaceShuttle(this);
    }

    @Override
    public void initAndPlay() {
        init();

        try {
            while (true) {
                if (invoker.isMyTurn() && isIsMyTurnNotFinished()) {
                    myTurnStarts = System.currentTimeMillis();
                    invoker.getActionCost();
                    if (!hasRounds) {
                        break;
                    }
                    myTurn();
                    setIsMyTurnNotFinished(false);
                }
            }
        } catch (InterruptedException e) {
            LOGGER.error("Exception", e);
        }
    }

    private void myTurn() {
        LOGGER.debug("myTurn started builderUnit: {}, Time: {}", actualBuilderId, myTurnStarts);
        if (isActionPointEnough(Action.WATCH) && availablePointsLeft > actionCost.get(Action.WATCH)) {
            invoker.watch(actualBuilderId);
            decreaseActionPoint(Action.WATCH);
        } else if (availablePointsLeft <= 0) {
            LOGGER.debug("Elfogyott a pénzünk, ezért nem wactholunk1!!!");
            return;
        }
        // Builderek úrhajóból indulnak, le kell csekkolni, hogy benne vannak-e még
        if (isBuilderCoordEqualsShutterCoord()) {
            LOGGER.debug("isBuilderCoordEqualsShutterCoord builderUnit: {}", actualBuilderId);
            if (gameMap.get(exitCoord).getType() == ObjectType.BUILDER_UNIT) {
                LOGGER.debug("Sajnos még vannak a kijaratban. BuilderUnit: {}", actualBuilderId);
                return;
            }

            if (gameMap.get(exitCoord).getType() == ObjectType.ROCK) {
                LOGGER.debug("exitCoord ROCK x: {}, y: {}", exitCoord.getX(), exitCoord.getY());
                //Ha van elég pénz, akkor fúrunk
                if (isActionPointEnough(Action.DRILL)) {
                    invoker.structureTunnel(actualBuilderId, calculateDirectonFromTargetCoordinate(exitCoord));
                    decreaseActionPoint(Action.DRILL);
                    if (isActionPointEnough(Action.MOVE)) {
                        invoker.moveBuilderUnit(actualBuilderId, calculateDirectonFromTargetCoordinate(exitCoord));
                        decreaseActionPoint(Action.MOVE);

                        //Kihozunk egy builder unitot az űrkompból
                        LOGGER.debug("Kihoztuk a {}. számú építőegységet az űrkompból!", actualBuilderId);
                        processActions();

                    }
                }
            } else if (gameMap.get(exitCoord).getType() == ObjectType.TUNNEL) {
                LOGGER.debug("exitCoord TUNNEL x: {}, y: {}", exitCoord.getX(), exitCoord.getY());
                if (isActionPointEnough(Action.MOVE)) {
                    invoker.moveBuilderUnit(actualBuilderId, calculateDirectonFromTargetCoordinate(exitCoord));
                    decreaseActionPoint(Action.MOVE);
                    if (isActionPointEnough(Action.WATCH) && availablePointsLeft > actionCost.get(Action.WATCH)) {
                        decreaseActionPoint(Action.WATCH);
                        invoker.watch(actualBuilderId);
                    } else if (availablePointsLeft <= 0) {
                        LOGGER.debug("Elfogyott a pénzünk, ezért nem wactholunk2!!!");
                        return;
                    }

                    //Kihozunk egy builder unitot az űrkompból
                    LOGGER.debug("Kihoztuk a {}. számú építőegységet az űrkompból!", actualBuilderId);
                    processActions();
                }
            }
        } else {
            //ha már nincs benne az űrhajóban
            //Űrhajón kívül indul a {}. számú építőegység!
            LOGGER.debug("Űrhajón kívül indul a {}. számú építőegység!", actualBuilderId);
            processActions();
        }

        drawMap();
    }

    private void processActions() {
        LOGGER.debug("processActions sikeresen meghívva!!! builderUnit: {}", actualBuilderId);
        ActionSequence actionSequence = null;
        while (true) {

            try {
                if (!invoker.isMyTurn()) {
                    break;
                }
            } catch (InterruptedException e) {
                LOGGER.error("InterruptedException", e);
                break;
            }

            if (isActionPointEnough(Action.WATCH) && availablePointsLeft > actionCost.get(Action.WATCH)) {
                decreaseActionPoint(Action.WATCH);
                if(!(invoker.watch(actualBuilderId))){
                    LOGGER.debug("Elfogyott az időnk, ezért nem wactholunk!!!");
                    break;
                }
            } else if (availablePointsLeft <= 0) {
                LOGGER.debug("Elfogyott a pénzünk, ezért nem wactholunk3!!!");
                break;
            }
            actionSequence = getActionForHaveEnoughMoney();
            if (actionSequence != null) {
                boolean successfulAction = true;
                for (Action actualAction : actionSequence.getListOfActions()) {
                    if (!successfulAction) {
                        LOGGER.debug("Előző végrehajtás sikertelen volt, ezért kilépünk");
                        break;
                    }
                    LOGGER.debug("Van még pénz!!! actualAction: {}", actualAction);
                    switch (actualAction) {
                        case EXPLODE:
                            if (invoker.explodeCell(actualBuilderId, calculateDirectonFromTargetCoordinate(actionSequence.getTargetCoordinate()))) {
                                LOGGER.debug("Sikeres művelet! : {}", actualAction);
                                decreaseActionPoint(Action.EXPLODE);
                            } else {
                                LOGGER.debug("Sikertelen művelet! : {}", actualAction);
                                int deadMoney = availablePointsLeft;
                                LOGGER.debug("Mi pénzünk magunk szerint. {}", availablePointsLeft);
                                invoker.getActionCost();
                                LOGGER.debug("Gép szerint a pénzünk. {}", availablePointsLeft);

                                if (deadMoney != availablePointsLeft) {
                                    LOGGER.error("Nem egyezik a pénz!!! Szerintünk: {}, Gép szerint: {}", deadMoney, availablePointsLeft);
                                }

                                successfulAction = false;
                            }
                            break;
                        case DRILL:
                            if (invoker.structureTunnel(actualBuilderId, calculateDirectonFromTargetCoordinate(actionSequence.getTargetCoordinate()))) {
                                LOGGER.debug("Sikeres művelet! : {}", actualAction);
                                decreaseActionPoint(Action.DRILL);
                            } else {
                                LOGGER.debug("Sikertelen művelet! : {}", actualAction);
                                int deadMoney = availablePointsLeft;
                                LOGGER.debug("Mi pénzünk magunk szerint. {}", availablePointsLeft);
                                invoker.getActionCost();
                                LOGGER.debug("Gép szerint a pénzünk. {}", availablePointsLeft);

                                if (deadMoney != availablePointsLeft) {
                                    LOGGER.error("Nem egyezik a pénz!!! Szerintünk: {}, Gép szerint: {}", deadMoney, availablePointsLeft);
                                }
                                successfulAction = false;
                            }
                            break;
                        case MOVE:
                            if (invoker.moveBuilderUnit(actualBuilderId, calculateDirectonFromTargetCoordinate(actionSequence.getTargetCoordinate()))) {
                                LOGGER.debug("Sikeres művelet! : {}", actualAction);
                                decreaseActionPoint(Action.MOVE);
                            } else {
                                LOGGER.debug("Sikertelen művelet! : {}", actualAction);
                                int deadMoney = availablePointsLeft;
                                LOGGER.debug("Mi pénzünk magunk szerint. {}", availablePointsLeft);
                                invoker.getActionCost();
                                LOGGER.debug("Gép szerint a pénzünk. {}", availablePointsLeft);

                                if (deadMoney != availablePointsLeft) {
                                    LOGGER.error("Nem egyezik a pénz!!! Szerintünk: {}, Gép szerint: {}", deadMoney, availablePointsLeft);
                                }
                                successfulAction = false;
                            }
                            break;
                        default:
                            LOGGER.error("Not expected element");
                            break;
                    }

                }

                if (!successfulAction) {
                    LOGGER.debug("Előző végrehajtás sikertelen volt, ezért kilépünk");
                    break;
                }

                if (actionSequence.getListOfActions().isEmpty()) {
                    LOGGER.debug("Nincs végrehajható művelet, mert csak ennyi {} pénzünk van!!!!", availablePointsLeft);
                    break;
                }
            } else {
                LOGGER.debug("Már nincs elég pénz!!!!");
                break;
            }
        }
    }

    private WsDirection calculateDirectonFromTargetCoordinate(Coordinate targetCoordinate) {
        WsCoordinate builderCoord = getUnit(actualBuilderId).getCord();

        if (builderCoord.getX() == targetCoordinate.getX()) {
            if (builderCoord.getY() > targetCoordinate.getY()) {
                return WsDirection.DOWN;
            } else {
                return WsDirection.UP;
            }

        } else if (builderCoord.getY() == targetCoordinate.getY()) {
            if (builderCoord.getX() > targetCoordinate.getX()) {
                return WsDirection.LEFT;
            } else {
                return WsDirection.RIGHT;
            }
        }

        return null;
    }

    private boolean isBuilderCoordEqualsShutterCoord() {
        WsCoordinate unitCoord = units.get(actualBuilderId).getCord();

        if (unitCoord.getX() == shuttlePos.getX() && unitCoord.getY() == shuttlePos.getY()) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isActionPointEnough(Action actionToBeDone) {
        LOGGER.debug("isActionPointEnough actionCost: {}, availablePointsLeft: {}, actionToBeDone: {}", actionCost.get(actionToBeDone), availablePointsLeft, actionToBeDone);
        if (availablePointsLeft - actionCost.get(actionToBeDone) >= 0) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isActionPointEnough(Integer cost) {
        LOGGER.debug("isActionPointEnough availablePointsLeft: {}", availablePointsLeft);
        if (availablePointsLeft - cost >= 0) {
            return true;
        } else {
            return false;
        }
    }

    private void decreaseActionPoint(Action actionToBeDone) {
        LOGGER.debug("decreaseActionPoint action: {}, before: {}", actionToBeDone, availablePointsLeft);
        availablePointsLeft -= actionCost.get(actionToBeDone);
        LOGGER.debug("decreaseActionPoint action: {}, after: {}", actionToBeDone, availablePointsLeft);
    }

    private ActionSequence getActionForHaveEnoughMoney() {
        WsCoordinate coordinate = getUnit(actualBuilderId).getCord();

        Coordinate coordinateLeft = new Coordinate(coordinate.getX() - 1, coordinate.getY());
        Coordinate coordinateDown = new Coordinate(coordinate.getX(), coordinate.getY() - 1);
        Coordinate coordinateRight = new Coordinate(coordinate.getX() + 1, coordinate.getY());
        Coordinate coordinateUp = new Coordinate(coordinate.getX(), coordinate.getY() + 1);

        CellInformation cellInformationLeft = gameMap.get(coordinateLeft);
        CellInformation cellInformationDown = gameMap.get(coordinateDown);
        CellInformation cellInformationRight = gameMap.get(coordinateRight);
        CellInformation cellInformationUp = gameMap.get(coordinateUp);
        
        LOGGER.debug("cellInformationLeft: {}", cellInformationLeft);
        LOGGER.debug("coordinateDown: {}", cellInformationDown);
        LOGGER.debug("coordinateRight: {}", cellInformationRight);
        LOGGER.debug("coordinateUp: {}", cellInformationUp);

        int cellInformationLeftMoney = Integer.MIN_VALUE;
        int cellInformationDownMoney = Integer.MIN_VALUE;
        int cellInformationRightMoney = Integer.MIN_VALUE;
        int cellInformationUpMoney = Integer.MIN_VALUE;

        if (cellInformationLeft != null) {
            cellInformationLeftMoney = objectTypeToMoney(cellInformationLeft);
        }

        if (cellInformationDown != null) {
            cellInformationDownMoney = objectTypeToMoney(cellInformationDown);
        }

        if (cellInformationRight != null) {
            cellInformationRightMoney = objectTypeToMoney(cellInformationRight);
        }

        if (cellInformationUp != null) {
            cellInformationUpMoney = objectTypeToMoney(cellInformationUp);
        }

        List<CoordinateAndMoney> priorityCoordinateAndMoneys = new ArrayList<CoordinateAndMoney>();
        //Csak akkor tesszük bele a mapbe, ha az action értéke legalább nulla, vagyis dolgozhatunk/léphetünk oda
        
        //sarkok vizsgálata
        if(ObjectType.OBSIDIAN == gameMap.get(coordinateLeft).getType() && ObjectType.OBSIDIAN == gameMap.get(coordinateDown).getType()){
            if(cellInformationUpMoney >=0 && isActionPointEnough(cellInformationUpMoney)){
                return new ActionSequence(coordinateUp, getActionsByObjectType(gameMap.get(coordinateUp)));
            }
        }
        
        if(ObjectType.OBSIDIAN == gameMap.get(coordinateLeft).getType() && ObjectType.OBSIDIAN == gameMap.get(coordinateUp).getType()){
            if(cellInformationRightMoney >=0 && isActionPointEnough(cellInformationRightMoney)){
                return new ActionSequence(coordinateRight, getActionsByObjectType(gameMap.get(coordinateRight)));
            }
        }
        
        if(ObjectType.OBSIDIAN == gameMap.get(coordinateRight).getType() && ObjectType.OBSIDIAN == gameMap.get(coordinateDown).getType()){
            if(cellInformationLeftMoney >=0 && isActionPointEnough(cellInformationLeftMoney)){
                return new ActionSequence(coordinateLeft, getActionsByObjectType(gameMap.get(coordinateLeft)));
            }
        }
        
        if(ObjectType.OBSIDIAN == gameMap.get(coordinateRight).getType() && ObjectType.OBSIDIAN == gameMap.get(coordinateUp).getType()){
            if(cellInformationDownMoney >=0 && isActionPointEnough(cellInformationDownMoney)){
                return new ActionSequence(coordinateDown, getActionsByObjectType(gameMap.get(coordinateDown)));
            }
        }
        
        //szélek vizsgálata
        if(ObjectType.OBSIDIAN == gameMap.get(coordinateLeft).getType()){
            if(cellInformationUpMoney >=0 && isActionPointEnough(cellInformationUpMoney)){
                return new ActionSequence(coordinateUp, getActionsByObjectType(gameMap.get(coordinateUp)));
            }
        }
        
        if(ObjectType.OBSIDIAN == gameMap.get(coordinateRight).getType()){
            if(cellInformationDownMoney >=0 && isActionPointEnough(cellInformationDownMoney)){
                return new ActionSequence(coordinateDown, getActionsByObjectType(gameMap.get(coordinateDown)));
            }
        }
        
        if(ObjectType.OBSIDIAN == gameMap.get(coordinateUp).getType()){
            if(cellInformationRightMoney >=0 && isActionPointEnough(cellInformationRightMoney)){
                return new ActionSequence(coordinateRight, getActionsByObjectType(gameMap.get(coordinateRight)));
            }
        }
        
        if(ObjectType.OBSIDIAN == gameMap.get(coordinateDown).getType()){
            if(cellInformationLeftMoney >=0 && isActionPointEnough(cellInformationLeftMoney)){
                return new ActionSequence(coordinateLeft, getActionsByObjectType(gameMap.get(coordinateLeft)));
            }
        }
        
        
        if (actualBuilderId % 4 == 0) {
            //West Builder Unit
            if (cellInformationLeftMoney >= 0) {
                priorityCoordinateAndMoneys.add(new CoordinateAndMoney(coordinateLeft, cellInformationLeftMoney));
            }
            if (cellInformationDownMoney >= 0) {
                priorityCoordinateAndMoneys.add(new CoordinateAndMoney(coordinateDown, cellInformationDownMoney));
            }
            if (cellInformationUpMoney >= 0) {
                priorityCoordinateAndMoneys.add(new CoordinateAndMoney(coordinateUp, cellInformationUpMoney));
            }
            if (cellInformationRightMoney >= 0) {
                priorityCoordinateAndMoneys.add(new CoordinateAndMoney(coordinateRight, cellInformationRightMoney));
            }
        } else if (actualBuilderId % 4 == 1) {
            //North Builder Unit
            if (cellInformationUpMoney >= 0) {
                priorityCoordinateAndMoneys.add(new CoordinateAndMoney(coordinateUp, cellInformationUpMoney));
            }
            if (cellInformationLeftMoney >= 0) {
                priorityCoordinateAndMoneys.add(new CoordinateAndMoney(coordinateLeft, cellInformationLeftMoney));
            }
            if (cellInformationRightMoney >= 0) {
                priorityCoordinateAndMoneys.add(new CoordinateAndMoney(coordinateRight, cellInformationRightMoney));
            }
            if (cellInformationDownMoney >= 0) {
                priorityCoordinateAndMoneys.add(new CoordinateAndMoney(coordinateDown, cellInformationDownMoney));
            }
        } else if (actualBuilderId % 4 == 2) {
            //East Builder Unit
            if (cellInformationRightMoney >= 0) {
                priorityCoordinateAndMoneys.add(new CoordinateAndMoney(coordinateRight, cellInformationRightMoney));
            }
            if (cellInformationUpMoney >= 0) {
                priorityCoordinateAndMoneys.add(new CoordinateAndMoney(coordinateUp, cellInformationUpMoney));
            }
            if (cellInformationDownMoney >= 0) {
                priorityCoordinateAndMoneys.add(new CoordinateAndMoney(coordinateDown, cellInformationDownMoney));
            }
            if (cellInformationLeftMoney >= 0) {
                priorityCoordinateAndMoneys.add(new CoordinateAndMoney(coordinateLeft, cellInformationLeftMoney));
            }
        } else if (actualBuilderId % 4 == 3) {
            //South Builder Unit
            if (cellInformationDownMoney >= 0) {
                priorityCoordinateAndMoneys.add(new CoordinateAndMoney(coordinateDown, cellInformationDownMoney));
            }
            if (cellInformationRightMoney >= 0) {
                priorityCoordinateAndMoneys.add(new CoordinateAndMoney(coordinateRight, cellInformationRightMoney));
            }
            if (cellInformationLeftMoney >= 0) {
                priorityCoordinateAndMoneys.add(new CoordinateAndMoney(coordinateLeft, cellInformationLeftMoney));
            }
            if (cellInformationUpMoney >= 0) {
                priorityCoordinateAndMoneys.add(new CoordinateAndMoney(coordinateUp, cellInformationUpMoney));
            }
        }

        return chooseNewAction(priorityCoordinateAndMoneys);

    }

    private ActionSequence chooseNewAction(List<CoordinateAndMoney> coordinateAndMoneys) {
        LOGGER.debug("chooseNewAction actionRanking merete: {}", coordinateAndMoneys != null ? coordinateAndMoneys.size() : null);

        if (coordinateAndMoneys == null || coordinateAndMoneys.isEmpty()) {
            return null;
        }

        //Rock
        for (CoordinateAndMoney actualCoordinateAndMoney : coordinateAndMoneys) {
            Coordinate coordinate = actualCoordinateAndMoney.getCoordinate();
            Integer maxCost = objectTypeToMoney(gameMap.get(coordinate));

            LOGGER.debug("chooseNewAction maxCostCoordinate: {}, maxCost: {}", coordinate, maxCost);

            if (ObjectType.ROCK == gameMap.get(coordinate).getType() && isActionPointEnough(maxCost)) {
                LOGGER.debug("chooseNewAction isActionPointEnough maxCost: {}", maxCost);
                return new ActionSequence(coordinate, getActionsByObjectType(gameMap.get(coordinate)));
            }
        }

        //Granit
        for (CoordinateAndMoney actualCoordinateAndMoney : coordinateAndMoneys) {
            Coordinate coordinate = actualCoordinateAndMoney.getCoordinate();
            Integer maxCost = objectTypeToMoney(gameMap.get(coordinate));

            LOGGER.debug("chooseNewAction maxCostCoordinate: {}, maxCost: {}", coordinate, maxCost);

            if (ObjectType.GRANITE == gameMap.get(coordinate).getType() && isActionPointEnough(maxCost)) {
                LOGGER.debug("chooseNewAction isActionPointEnough maxCost: {}", maxCost);
                return new ActionSequence(coordinate, getActionsByObjectType(gameMap.get(coordinate)));
            }
        }
        
        //Ellenseg robbantasa
        for (CoordinateAndMoney actualCoordinateAndMoney : coordinateAndMoneys) {
            Coordinate coordinate = actualCoordinateAndMoney.getCoordinate();
            Integer maxCost = objectTypeToMoney(gameMap.get(coordinate));

            LOGGER.debug("chooseNewAction maxCostCoordinate: {}, maxCost: {}", coordinate, maxCost);

            if (ObjectType.TUNNEL == gameMap.get(coordinate).getType() && gameMap.get(coordinate).isEnemy() && isActionPointEnough(maxCost)) {
                LOGGER.debug("chooseNewAction isActionPointEnough maxCost: {}", maxCost);
                return new ActionSequence(coordinate, getActionsByObjectType(gameMap.get(coordinate)));
            }
        }

        //Move
        for (CoordinateAndMoney actualCoordinateAndMoney : coordinateAndMoneys) {
            Coordinate coordinate = actualCoordinateAndMoney.getCoordinate();
            Integer maxCost = objectTypeToMoney(gameMap.get(coordinate));

            LOGGER.debug("chooseNewAction maxCostCoordinate: {}, maxCost: {}", coordinate, maxCost);

            if (ObjectType.TUNNEL == gameMap.get(coordinate).getType() && !(gameMap.get(coordinate).isEnemy()) && isActionPointEnough(maxCost)) {
                LOGGER.debug("chooseNewAction isActionPointEnough maxCost: {}", maxCost);
                return new ActionSequence(coordinate, getActionsByObjectType(gameMap.get(coordinate)));
            }
        }

        LOGGER.debug("chooseNewAction Nincs elég pénz semmire builderUnit: {}", actualBuilderId);
        return null;

    }

    private List<Action> getActionsByObjectType(CellInformation cellinformation) {
        LOGGER.debug("getActionsByObjectType objectType: {}", cellinformation.getType());
        List<Action> response = new ArrayList<Action>();
        switch (cellinformation.getType()) {
            case TUNNEL:
                if (cellinformation.isEnemy()) {
                    response.add(Action.EXPLODE);
                } else {
                    response.add(Action.MOVE);
                }
                break;
            case SHUTTLE:
                break;
            case BUILDER_UNIT:
                break;
            case ROCK:
                response.add(Action.DRILL);
                //response.add(Action.MOVE);
                break;
            case GRANITE:
                response.add(Action.EXPLODE);
                //response.add(Action.DRILL);
                //response.add(Action.MOVE);
                break;
            case OBSIDIAN:
                break;
            default:
                break;
        }
        LOGGER.debug("getActionsByObjectType response size: {}", response.size());
        return response;
    }

    protected int objectTypeToMoney(CellInformation cellinformation) {
        LOGGER.debug("objectTypeToMoney objectType: {}", cellinformation.getType());
        switch (cellinformation.getType()) {
            case TUNNEL:
                if (cellinformation.isEnemy()) {
                    LOGGER.debug("Enemyt találtam");
                    return actionCost.get(Action.EXPLODE);
                } else {
                    return actionCost.get(Action.MOVE);
                }
            case SHUTTLE:
                return Integer.MIN_VALUE;
            case BUILDER_UNIT:
                return Integer.MIN_VALUE;
            case ROCK:
                return actionCost.get(Action.DRILL);
            case GRANITE:
                return actionCost.get(Action.EXPLODE);
            case OBSIDIAN:
                return Integer.MIN_VALUE;
            default:
                return Integer.MIN_VALUE;
        }
    }

    private void drawMap() {

        for (Map.Entry<Integer, WsBuilderunit> entry : units.entrySet()) {
            LOGGER.debug("BuilderUnit id: {}, X: {}, Y: {}", entry.getKey(), entry.getValue().getCord().getX(), entry.getValue().getCord().getY());
            gameMap.put(new Coordinate(entry.getValue().getCord().getX(), entry.getValue().getCord().getY()), new CellInformation(ObjectType.BUILDER_UNIT, "javatars"));
        }

        for (int i = 0; i < 29; i++) {
            for (int j = 0; j < 53; j++) {
                CellInformation inf = gameMap.get(new Coordinate(j, i));
                if (inf != null) {
                    switch (inf.getType()) {
                        case OBSIDIAN:
                            System.out.print("O");
                            break;
                        case BUILDER_UNIT:
                            System.out.print("B");
                            break;
                        case GRANITE:
                            System.out.print("G");
                            break;
                        case ROCK:
                            System.out.print("R");
                            break;
                        case TUNNEL:
                            System.out.print("T");
                            break;
                        case SHUTTLE:
                            System.out.print("S");
                            break;

                    }
                    System.out.print(" ");
                } else {
                    System.out.print("X ");
                }
                if (j == 52) {
                    System.out.println();
                }
            }

        }
    }
}
