package hu.javachallenge.javatars.algorithms;

import eu.loxon.centralcontrol.WsBuilderunit;
import hu.javachallenge.javatars.accessor.ManagedOperationInvoker;
import hu.javachallenge.javatars.util.Action;
import hu.javachallenge.javatars.util.CellInformation;
import hu.javachallenge.javatars.util.Coordinate;

import java.util.Map;

public abstract class SpaceShuttle {

    protected ManagedOperationInvoker invoker;

    protected Map<Integer, WsBuilderunit> units;
    protected Coordinate exitCoord;
    protected Coordinate shuttlePos;
    protected Map<Action, Integer> actionCost;
    protected Map<Coordinate, CellInformation> gameMap;
    protected Coordinate size;
    protected int availablePointsLeft;
    protected int availableExplosives;
    protected int actualBuilderId;
    protected boolean hasRounds;
    protected long myTurnStarts;
    protected boolean isMyTurnNotFinished;

    public abstract void initAndPlay();

    /**
     * Inicializálja a játékot.<br>
     * Meghívásra kerülnek az alábbi kérések:<br>
     * StartGameRequest
     * GetSpaceShuttlePosRequest
     * GetSpaceShuttleExitPosRequest
     */
    public void init() {
        invoker.startGame();
        invoker.setSpaceShuttlePos();
        invoker.setSpaceShuttleExitPos();        
    }

    public ManagedOperationInvoker getInvoker() {
        return invoker;
    }

    public void setInvoker(ManagedOperationInvoker invoker) {
        this.invoker = invoker;
    }

    public WsBuilderunit getUnit(int unit) {
        return units.get(new Integer(unit));
    }

    public void putUnit(int unitNum, WsBuilderunit unit) {
        this.units.put(unitNum, unit);
    }

    public Coordinate getExitCoord() {
        return exitCoord;
    }

    public void setExitCoord(Coordinate exitCoord) {
        this.exitCoord = exitCoord;
    }

    public Coordinate getShuttlePos() {
        return shuttlePos;
    }

    public void setShuttlePos(Coordinate shuttlePos) {
        this.shuttlePos = shuttlePos;
    }

    public Map<Action, Integer> getActionCost() {
        return actionCost;
    }

    public void setActionCost(Map<Action, Integer> actionCost) {
        this.actionCost = actionCost;
    }

    public CellInformation getCell(Coordinate coord) {
        return gameMap.get(coord);
    }

    public void putCellInformation(Coordinate coord, CellInformation cell) {
        this.gameMap.put(coord, cell);
    }

    public int getAvailablePointsLeft() {
        return availablePointsLeft;
    }

    public void setAvailablePointsLeft(int availablePointsLeft) {
        this.availablePointsLeft = availablePointsLeft;
    }

    public int getAvailableExplosives() {
        return availableExplosives;
    }

    public void setAvailableExplosives(int availableExplosives) {
        this.availableExplosives = availableExplosives;
    }

    public Coordinate getSize() {
        return size;
    }

    public void setSize(Coordinate size) {
        this.size = size;
    }

    public int getActualBuilderId() {
        return actualBuilderId;
    }

    public void setActualBuilderId(int actualBuilderId) {
        this.actualBuilderId = actualBuilderId;
    }

    public boolean isHasRounds() {
        return hasRounds;
    }

    public void setHasRounds(boolean hasRounds) {
        this.hasRounds = hasRounds;
    }

    public boolean isIsMyTurnNotFinished() {
        return isMyTurnNotFinished;
    }

    public void setIsMyTurnNotFinished(boolean isMyTurnNotFinished) {
        this.isMyTurnNotFinished = isMyTurnNotFinished;
    }

    public long getMyTurnStarts() {
        return myTurnStarts;
    }

    public void setMyTurnStarts(long myTurnStarts) {
        this.myTurnStarts = myTurnStarts;
    }
    
    
    
}
