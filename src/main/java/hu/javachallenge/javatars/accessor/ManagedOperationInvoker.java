package hu.javachallenge.javatars.accessor;

import eu.loxon.centralcontrol.*;
import hu.javachallenge.javatars.algorithms.SpaceShuttle;
import hu.javachallenge.javatars.util.Action;
import hu.javachallenge.javatars.util.CellInformation;
import hu.javachallenge.javatars.util.Coordinate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class ManagedOperationInvoker {

    private CentralControlServiceService server;
    private SpaceShuttle shuttle;
    private long isMyTurnLastInvoked = 0;
    private static final Logger LOGGER = LoggerFactory.getLogger(ManagedOperationInvoker.class);

    /**
     * Meghívja a serveren a radar kérést, majd a visszaadott válaszból
     * kigyűjtött információkkal feltölti a shuttle objektumot.<br>
     * <br>
     * Megjegyzés:<br>
     * a {@link #checkRadar(List, WsCoordinate)} Kiveszi a paraméterként kapott
     * listából azokat a coordinátákat, amelyek a hatókörön kívül esnek ezért
     * nem kaphatunk büntetőpontot.
     */
    public void radar(int unit, List<Coordinate> coords) {
        List<WsCoordinate> scanCells = null;
        //scanCells = checkRadar(coords, shuttle.getUnit(unit).getCord());

        RadarRequest request = new RadarRequest();
        request.setUnit(unit);

        for (WsCoordinate coord : scanCells) {
            request.getCord().add(coord);
        }

        RadarResponse response = server.getCentralControlPort().radar(request);

        if (response.getResult().getType() == ResultType.DONE) {

            LOGGER.info("RadarRequest successfully processed");

            for (Scouting coord : response.getScout()) {
                shuttle.putCellInformation(new Coordinate(coord.getCord()), new CellInformation(coord.getObject(), coord.getTeam()));
            }

        } else if (response.getResult().getType() == ResultType.INVALID) {

            LOGGER.warn("Invalid RadarRequest");
            LOGGER.warn(response.getResult().getMessage());

        } else {

            LOGGER.error("Error in central control");

        }

    }

    /**
     * Meghívja a serveren a getActionCost kérést, majd a visszaadott válaszból
     * kigyűjtött információkkal feltölti a shuttle objektumot.<br>
     */
    public void getActionCost() {
        ActionCostRequest request = new ActionCostRequest();

        ActionCostResponse response = server.getCentralControlPort().getActionCost(request);

        if (response.getResult().getType() == ResultType.DONE) {

            LOGGER.info("ActionCostResponse successfully processed");

            ///TODO - törölni. Csak debug célból h hány kör van hátra.
            LOGGER.info("Hátralévö körök: {}", response.getResult().getTurnsLeft());
            LOGGER.info("Actual BuilderID: {}", response.getResult().getBuilderUnit());
            LOGGER.info("Actual Total Point: {}", response.getResult().getScore().getTotal());

            shuttle.setHasRounds(response.getResult().getTurnsLeft() != 0);

            LOGGER.debug("shuttle.getActionCost() sizeBefore: {}", shuttle.getActionCost().size());

            LOGGER.debug("Action.DRILL: {}", response.getDrill());
            LOGGER.debug("Action.EXPLODE: {}", response.getExplode());
            LOGGER.debug("Action.MOVE: {}", response.getMove());
            LOGGER.debug("Action.RADAR: {}", response.getRadar());
            LOGGER.debug("Action.WATCH: {}", response.getWatch());

            shuttle.getActionCost().put(Action.DRILL, response.getDrill());
            shuttle.getActionCost().put(Action.EXPLODE, response.getExplode());
            shuttle.getActionCost().put(Action.MOVE, response.getMove());
            shuttle.getActionCost().put(Action.RADAR, response.getRadar());
            shuttle.getActionCost().put(Action.WATCH, response.getWatch());

            LOGGER.debug("shuttle.getActionCost() sizeAfter: {}", shuttle.getActionCost().size());

            shuttle.setAvailableExplosives(response.getAvailableExplosives());
            shuttle.setAvailablePointsLeft(response.getAvailableActionPoints());

        } else if (response.getResult().getType() == ResultType.INVALID) {

            LOGGER.warn("Invalid ActionCostResponse");
            LOGGER.warn(response.getResult().getMessage());

        } else {

            LOGGER.error("Error in central control");

        }
    }

    /**
     * Meghívja a serveren az explodeCell kérést.<br>
     * <br>
     * Megjegyzés:<br> {@link #checkExplode(WsDirection, WsCoordinate)} metódus
     * ellenőrzi a kérést.
     */
    public boolean explodeCell(int unit, WsDirection dir) {

        if(checkRoundIsOver(unit)){
            return false;
        }

        ExplodeCellRequest request = new ExplodeCellRequest();
        ExplodeCellResponse response = new ExplodeCellResponse();
        request.setUnit(unit);
        request.setDirection(dir);
        Coordinate targetCell = new Coordinate(shuttle.getUnit(unit).getCord(), dir);

        //checkExplode(dir, shuttle.getUnit(unit).getCord());

        response = server.getCentralControlPort().explodeCell(request);

        if (response.getResult().getType() == ResultType.DONE) {

            LOGGER.info("ExplodeCellRequest successfully processed");

            shuttle.getCell(targetCell).setType(ObjectType.ROCK);
            shuttle.getCell(targetCell).setEnemy(false);

            return true;

        } else if (response.getResult().getType() == ResultType.INVALID) {

            LOGGER.warn("Invalid ExplodeCellRequest");
            LOGGER.warn(response.getResult().getMessage());

        } else {

            LOGGER.error("Error in central control");

        }
        return false;
    }

    /**
     * Meghívja a serveren a getSpaceShuttlePos kérést, majd a visszatért
     * válasszal feltölti a shuttle objektumot.<br>
     * <br>
     * Megjegyzés: <br>
     * Ezt a metódust a spaceshuttle(saját algoritmusunk) implementációjából
     * célszerűtlen meghívni, ugyanis a
     * {@link hu.javachallenge.javatars.algorithms.SpaceShuttle SpaceShuttle}
     * abstract osztály
     * {@link hu.javachallenge.javatars.algorithms.SpaceShuttle#init() init()}
     * metódusában egyszer meghívásra kerül ezután pedig a shuttle objektumból
     * elérhető a shuttle pozíciója.
     */
    public void setSpaceShuttlePos() {

        GetSpaceShuttlePosRequest request = new GetSpaceShuttlePosRequest();
        GetSpaceShuttlePosResponse response;

        response = server.getCentralControlPort().getSpaceShuttlePos(request);

        if (response.getResult().getType() == ResultType.DONE) {

            LOGGER.info("GetSpaceShuttlePosRequest successfully processed");

            shuttle.setShuttlePos(new Coordinate(response.getCord()));

        } else if (response.getResult().getType() == ResultType.INVALID) {

            LOGGER.warn("Invalid GetSpaceShuttlePosRequest");
            LOGGER.warn(response.getResult().getMessage());

        } else {

            LOGGER.error("Error in central control");

        }
    }

    /**
     * Meghívja a serveren a getSpaceShuttleExitPos kérést, majd a visszatért
     * válasszal feltölti a shuttle objektumot.<br>
     * <br>
     * Megjegyzés: <br>
     * Ezt a metódust a spaceshuttle(saját algoritmusunk) implementációjából
     * célszerűtlen meghívni, ugyanis a
     * {@link hu.javachallenge.javatars.algorithms.SpaceShuttle SpaceShuttle}
     * abstract osztály
     * {@link hu.javachallenge.javatars.algorithms.SpaceShuttle#init() init()}
     * metódusában egyszer meghívásra kerül ezután pedig a shuttle objektumból
     * elérhető a shuttle kijáratának pozíciója.
     */
    public void setSpaceShuttleExitPos() {

        GetSpaceShuttleExitPosRequest request = new GetSpaceShuttleExitPosRequest();
        GetSpaceShuttleExitPosResponse response;

        response = server.getCentralControlPort().getSpaceShuttleExitPos(request);

        if (response.getResult().getType() == ResultType.DONE) {

            LOGGER.info("GetSpaceShuttleExitPosRequest successfully processed");

            shuttle.setExitCoord(new Coordinate(response.getCord()));

        } else if (response.getResult().getType() == ResultType.INVALID) {

            LOGGER.warn("Invalid GetSpaceShuttleExitPosRequest");
            LOGGER.warn(response.getResult().getMessage());

        } else {

            LOGGER.error("Error in central control");

        }
    }

    /**
     * Meghívja a serveren a structureTunnel kérést. Frissíti a shuttle-ben
     * tárol map-et<br>
     * <br>
     * Megjegyzés:<br> {@link #checkStructureTunnel(WsDirection, WsCoordinate)}
     * metódus ellenőrzi a kérést.
     */
    public boolean structureTunnel(int unit, WsDirection dir) {

        if(checkRoundIsOver(unit)){
            return false;
        }

        StructureTunnelRequest request = new StructureTunnelRequest();
        StructureTunnelResponse response = new StructureTunnelResponse();
        request.setUnit(unit);
        request.setDirection(dir);
        Coordinate targetCell = new Coordinate(shuttle.getUnit(unit).getCord(), dir);

        //checkStructureTunnel(shuttle.getUnit(unit).getCord(), dir);

        response = server.getCentralControlPort().structureTunnel(request);

        if (response.getResult().getType() == ResultType.DONE) {

            LOGGER.info("StructureTunnelRequest successfully processed");

            shuttle.getCell(targetCell).setType(ObjectType.TUNNEL);
            shuttle.getCell(targetCell).setEnemy(false);

            return true;

        } else if (response.getResult().getType() == ResultType.INVALID) {

            LOGGER.warn("Invalid StructureTunnelRequest");
            LOGGER.warn(response.getResult().getMessage());
            return false;
        } else {
            LOGGER.error("Error in central control");
            return false;
        }
    }

    /**
     * Meghívja a serveren a watch kérést, majd a visszaadott válaszból
     * kigyűjtött információkkal feltölti a shuttle objektumot.<br>
     * <br>
     *
     * @param unit aktuális builder unit száma
     */
    public boolean watch(int unit) {

        if(checkRoundIsOver(unit)){
            return false;
        }

        WatchRequest request = new WatchRequest();
        request.setUnit(unit);

        WatchResponse response = server.getCentralControlPort().watch(request);

        if (response.getResult().getType() == ResultType.DONE) {

            LOGGER.info("WatchRequest successfully processed");

            for (Scouting coord : response.getScout()) {
                LOGGER.debug("CellInformation x: {}, y:{}, object: {}, team: {}", coord.getCord().getX(), coord.getCord().getY(), coord.getObject(), coord.getTeam());
                shuttle.putCellInformation(new Coordinate(coord.getCord()), new CellInformation(coord.getObject(), coord.getTeam()));
            }

            LOGGER.debug("watch after ActioonPointsLeft: {}", response.getResult().getActionPointsLeft());
            return true;

        } else if (response.getResult().getType() == ResultType.INVALID) {

            LOGGER.warn("Invalid WatchRequest");
            LOGGER.warn(response.getResult().getMessage());

        } else {

            LOGGER.error("Error in central control");

        }
        return false;

    }

    /**
     * Meghívja a serveren az isMyTurn kérést, majd visszatér a válasszal.<br>
     * Hibás kérés esetén false-al tér vissza.<br>
     * <br>
     * Megjegyzés:<br>
     * Ha két kérés között kevesebb mint 300 ms telt el, akkor a metódus blokkol
     * és kivárja a szükséges időt.
     */
    public boolean isMyTurn() throws InterruptedException {
        IsMyTurnRequest request = new IsMyTurnRequest();
        IsMyTurnResponse response = new IsMyTurnResponse();

        long elapsedTime = System.currentTimeMillis() - isMyTurnLastInvoked;

        if (elapsedTime > 160) {
            response = server.getCentralControlPort().isMyTurn(request);
            isMyTurnLastInvoked = System.currentTimeMillis();
        } else {
            Thread.sleep(160 - elapsedTime);
            response = server.getCentralControlPort().isMyTurn(request);
            isMyTurnLastInvoked = System.currentTimeMillis();
        }

        if (response.getResult().getType() == ResultType.DONE) {

            if (shuttle.getActualBuilderId() != response.getResult().getBuilderUnit()) {
                shuttle.setIsMyTurnNotFinished(true);
            }

            shuttle.setActualBuilderId(response.getResult().getBuilderUnit());

            LOGGER.info("IsMyTurnRequest successfully processed: {}", response.isIsYourTurn());
            return response.isIsYourTurn();

        } else if (response.getResult().getType() == ResultType.INVALID) {

            LOGGER.warn("Invalid IsMyTurnRequest");
            LOGGER.warn(response.getResult().getMessage());
            return false;

        } else {

            LOGGER.error("Error in central control");
            return false;

        }
    }

    /**
     * Meghívja a serveren a MoveBuilderUnit kérést.<br>
     * <br>
     * Megjegyzés:<br> {@link #checkMoveBuilderUnit(WsDirection, WsCoordinate)}
     * metódus ellenőrzi a kérést.
     */
    public boolean moveBuilderUnit(int unit, WsDirection dir) {

        if(checkRoundIsOver(unit)){
            return false;
        }

        MoveBuilderUnitRequest request = new MoveBuilderUnitRequest();
        MoveBuilderUnitResponse response;
        request.setUnit(unit);
        request.setDirection(dir);
        Coordinate nextCell = new Coordinate(shuttle.getUnit(unit).getCord(), dir);

        //checkMoveBuilderUnit(shuttle.getUnit(unit).getCord(), dir);

        response = server.getCentralControlPort().moveBuilderUnit(request);

        if (response.getResult().getType() == ResultType.DONE) {

            LOGGER.info("MoveBuilderUnitRequest successfully processed");

            shuttle.getUnit(unit).setCord(nextCell);

            return true;
        } else if (response.getResult().getType() == ResultType.INVALID) {

            LOGGER.warn("Invalid MoveBuilderUnitRequest");
            LOGGER.warn(response.getResult().getMessage());

        } else {

            LOGGER.error("Error in central control");

        }
        return false;
    }

    /**
     * Meghívja a serveren a StartGame kérést, majd a visszatért válasszal
     * feltölti a shuttle objektumot.<br>
     * <br>
     * Megjegyzés: <br>
     * Ezt a metódust a spaceshuttle(saját algoritmusunk) implementációjából
     * célszerűtlen meghívni, ugyanis a
     * {@link hu.javachallenge.javatars.algorithms.SpaceShuttle SpaceShuttle}
     * abstract osztály
     * {@link hu.javachallenge.javatars.algorithms.SpaceShuttle#init() init()}
     * metódusában egyszer meghívásra kerül.
     */
    public void startGame() {
        StartGameRequest request = new StartGameRequest();

        StartGameResponse response = server.getCentralControlPort().startGame(request);

        if (response.getResult().getType() == ResultType.DONE) {

            LOGGER.info("StartGameRequest successfully processed");

            shuttle.setAvailableExplosives(response.getResult().getExplosivesLeft());
            shuttle.setAvailablePointsLeft(response.getResult().getActionPointsLeft());
            
            LOGGER.debug("response.getSize() X: {}, Y: {}", response.getSize().getX(), response.getSize().getY());

            for (WsBuilderunit unit : response.getUnits()) {
                shuttle.putUnit(unit.getUnitid(), unit);
            }

            shuttle.setSize(new Coordinate(response.getSize()));
        } else if (response.getResult().getType() == ResultType.INVALID) {

            LOGGER.warn("Invalid StartGameRequest");
            LOGGER.warn(response.getResult().getMessage());

        } else {

            LOGGER.error("Error in central control");

        }
    }

    public void setSpaceShuttle(SpaceShuttle shuttle) {
        this.shuttle = shuttle;
    }

    public void setServer(CentralControlServiceService server) {
        this.server = server;
    }
    
    private boolean checkRoundIsOver(int unit){
        long elapsedTime = System.currentTimeMillis() - shuttle.getMyTurnStarts();
        LOGGER.debug("processActions BuilderUnit: {}, elapsedTime: {}", unit, elapsedTime);
        //Check the timeout
        if (elapsedTime > 700) {
            LOGGER.debug("Letelt a {}. számú builder unit építési ideje!!!", unit);
            return true;
        }
        return false;
    }

}
