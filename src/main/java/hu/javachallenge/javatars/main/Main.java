package hu.javachallenge.javatars.main;

import java.net.Authenticator;
import java.net.MalformedURLException;
import java.net.PasswordAuthentication;

import eu.loxon.centralcontrol.CentralControlServiceService;
import hu.javachallenge.javatars.accessor.ManagedOperationInvoker;
import hu.javachallenge.javatars.algorithms.SimpleSpaceShuttle;
import java.net.URL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) throws MalformedURLException {
        if (args.length == 3) {
            Authenticator.setDefault(new Authenticator() {

                private String user = args[1];
                private String password = args[2];

                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    PasswordAuthentication auth = new PasswordAuthentication(user, password.toCharArray());
                    return auth;
                }
            });

            CentralControlServiceService service = new CentralControlServiceService(new URL(args[0]));

            ManagedOperationInvoker invoker = new ManagedOperationInvoker();
            invoker.setServer(service);
            new SimpleSpaceShuttle(invoker).initAndPlay();
        }else{
            LOGGER.error("Nem megfelelő paraméterszám!!!!");
        }
    }
}