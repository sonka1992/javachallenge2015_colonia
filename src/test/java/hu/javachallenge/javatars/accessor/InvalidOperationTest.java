package hu.javachallenge.javatars.accessor;

import java.util.Arrays;

import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import eu.loxon.centralcontrol.ObjectType;
import eu.loxon.centralcontrol.WsBuilderunit;
import eu.loxon.centralcontrol.WsCoordinate;
import eu.loxon.centralcontrol.WsDirection;
import hu.javachallenge.javatars.algorithms.SimpleSpaceShuttle;
import hu.javachallenge.javatars.algorithms.SpaceShuttle;
import hu.javachallenge.javatars.exceptions.InvalidOperationException;
import hu.javachallenge.javatars.exceptions.NoCellInformationException;
import hu.javachallenge.javatars.util.CellInformation;
import hu.javachallenge.javatars.util.Coordinate;
import hu.javachallenge.javatars.util.ExceptionMessage;
import org.junit.Ignore;

@RunWith(Parameterized.class)
public class InvalidOperationTest {
	
	private static SpaceShuttle shuttle;
	private int unitId;
	private WsDirection dir;
	private Exception expectedException;
	private ExceptionMessage msg;
	private Function<?, ?> function;
	
	@Rule
	public final ExpectedException exception = ExpectedException.none();
	
	
	public InvalidOperationTest(int unitId, WsDirection dir, 
			Exception expectedException, ExceptionMessage msg,
			Function<?, ?> function) {
		
		this.unitId = unitId;
		this.dir = dir;
		this.expectedException = expectedException;
		this.msg = msg;
		this.function = function;
	}


	@BeforeClass
	public static void init(){
		shuttle = new SimpleSpaceShuttle(new ManagedOperationInvoker());
		shuttle.setAvailableExplosives(4);
		shuttle.setAvailablePointsLeft(40);
		WsBuilderunit unit = new WsBuilderunit();
		WsCoordinate unitPos = new WsCoordinate();
		unitPos.setX(30);
		unitPos.setY(30);
		unit.setUnitid(1);
		unit.setCord(unitPos);
		
		shuttle.putUnit(1,unit);
		
		shuttle.putCellInformation(new Coordinate(29,30), 
				new CellInformation(ObjectType.TUNNEL,"enemy"));
		shuttle.putCellInformation(new Coordinate(31,30), 
				new CellInformation(ObjectType.ROCK,""));
		shuttle.putCellInformation(new Coordinate(30,31), 
				new CellInformation(ObjectType.OBSIDIAN,""));
		

		WsBuilderunit unit2 = new WsBuilderunit();
		WsCoordinate unit2Pos = new WsCoordinate();
		unit2Pos.setX(40);
		unit2Pos.setY(40);
		unit2.setUnitid(2);
		unit2.setCord(unit2Pos);
		
		shuttle.putUnit(2,unit2);

		shuttle.putCellInformation(new Coordinate(39,40), 
				new CellInformation(ObjectType.BUILDER_UNIT,"enemy"));
		shuttle.putCellInformation(new Coordinate(41,40), 
				new CellInformation(ObjectType.SHUTTLE,"enemy"));
		shuttle.putCellInformation(new Coordinate(40,41), 
				new CellInformation(ObjectType.GRANITE,""));
	}
	
	@FunctionalInterface
	interface Function<One, Two> {
	    public void apply(int unitId, WsDirection dir);
	}
	
	@Parameters
	public static Iterable<Object[]> data1() {
		Function<Integer,WsDirection> checkExplode = (x,y) -> shuttle.getInvoker().explodeCell(x, y);
		Function<Integer,WsDirection> checkMove = (x,y) -> shuttle.getInvoker().moveBuilderUnit(x, y);
		Function<Integer,WsDirection> checkStructure = (x,y) -> shuttle.getInvoker().structureTunnel(x, y);
		
		// Paraméterek: 
		//1. Builder unit id-je
		//2. Builder unit iránya
		//3. Várt exception
		//4. Várt exception üzenet
		//5. Tesztelt metódus
		
		return Arrays.asList(new Object[][] { 
			
			// check explode
			{ 1,
				WsDirection.DOWN, 
				new NoCellInformationException(), 
				ExceptionMessage.NoCellInformation, 
				checkExplode},
			{ 1,
				WsDirection.LEFT, 
				new InvalidOperationException(), 
				ExceptionMessage.AlreadyExplodedCell, 
				checkExplode},
			{ 1,
				WsDirection.RIGHT, 
				new InvalidOperationException(), 
				ExceptionMessage.AlreadyExplodedCell, 
				checkExplode},
			{ 1,
				WsDirection.UP, 
				new InvalidOperationException(), 
				ExceptionMessage.CannotExplodeObsidian, 
				checkExplode},
			{ 2,
				WsDirection.LEFT, 
				new InvalidOperationException(), 
				ExceptionMessage.BlockedCell, 
				checkExplode},
			{ 2,
				WsDirection.RIGHT, 
				new InvalidOperationException(), 
				ExceptionMessage.BlockedCell, 
				checkExplode},
			
			//check move
			{ 1,
				WsDirection.DOWN, 
				new NoCellInformationException(), 
				ExceptionMessage.NoCellInformation, 
				checkMove},
			{ 1,
				WsDirection.LEFT, 
				new InvalidOperationException(), 
				ExceptionMessage.EnemyTerritory, 
				checkMove},
			{ 1,
				WsDirection.RIGHT, 
				new InvalidOperationException(), 
				ExceptionMessage.UnreachableCell, 
				checkMove},
			{ 1,
				WsDirection.UP, 
				new InvalidOperationException(), 
				ExceptionMessage.UnreachableCell, 
				checkMove},
			{ 2,
				WsDirection.UP, 
				new InvalidOperationException(), 
				ExceptionMessage.UnreachableCell, 
				checkMove},
			{ 2,
				WsDirection.LEFT, 
				new InvalidOperationException(), 
				ExceptionMessage.BlockedCell, 
				checkExplode},
			{ 2,
				WsDirection.RIGHT, 
				new InvalidOperationException(), 
				ExceptionMessage.BlockedCell, 
				checkExplode},
			
			//check structure
			{ 1,
				WsDirection.DOWN, 
				new NoCellInformationException(), 
				ExceptionMessage.NoCellInformation, 
				checkStructure},
			{ 1,
				WsDirection.LEFT, 
				new InvalidOperationException(), 
				ExceptionMessage.AlreadyStructuredCell, 
				checkStructure},
			{ 2,
				WsDirection.UP, 
				new InvalidOperationException(), 
				ExceptionMessage.CannotStructureGranite, 
				checkStructure},
			{ 2,
				WsDirection.RIGHT, 
				new InvalidOperationException(), 
				ExceptionMessage.CannotStructureShuttle, 
				checkStructure}
		});
	}
	
        @Ignore
	@Test
	public void invalidOperationTests(){
		exception.expect(expectedException.getClass());
		exception.expectMessage(msg.getMessage());
		
		function.apply(unitId, dir);
	}
	
}
